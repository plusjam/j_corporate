<?php
/**
 * Template Name: Works
 */
get_header() ;?>
	<div class="wrapper">

<!-- container -->
<div class="container works">

    <!-- lead -->
    <section class="lead">
        <div class="inner-1620">
        <div class="multi-title js-anime">
            <h1><span>W</span><span>o</span><span>r</span><span>k</span><span>s</span></h1>
            <p>事例紹介</p>
        </div>
        </div>
    </section>
    <!-- /lead -->

    <!-- main -->
    <div class="main">
        <div class="main-head">
        <?php
            // 親カテゴリーのものだけを一覧で取得
            $args = array(
                'parent' => 0,
                'orderby' => 'term_order',
                // 'order' => 'ASC'
            );
            $categories = get_categories( $args );
        ?>
            <ul class="main-category js-anime">
            <li class=""><div><a>All</a></div></li>
            <?php foreach( $categories as $category ) : ?>
                <li>
		            <div><?php echo $category->name; ?></div>
	            </li>
            <?php endforeach; ?>
            </ul>
        </div>
        <div class="inner-1100">
            <ul class="works-list" id="">
            <?php $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

            if (!empty($_GET['all'])) {
                // $_GET['all']に値が入っている場合
                $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => '12',
                    'paged' => $paged,
                );

            } else {
                $num = $paged * 12;
                $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => $num, //ページ数×12投稿　読込
                    'paged' => 1,

                );

            }
            $wp_query = new WP_Query($args);
            ?>
            <?php if ($wp_query->have_posts() ) : ?>
            <?php while ($wp_query->have_posts() ) : $wp_query->the_post(); ?>
                <li class="works-item js-anime">
                    <a href="<?php the_permalink(); ?>">
                        <div>
                            <?php 
                            $pcThumbnail = get_field('pc_thumbnail');
                            if($pcThumbnail){ 
                            ?>
                                <img src="<?php echo esc_url($pcThumbnail['url']) ?>" alt="<?php echo esc_attr($pcThumbnail['alt']) ?>">
                                    
                            <?php }; ?>
                        </div>
                        <dl>
                            <dt><?php the_title(); ?></dt>
                            <dd><?php echo get_post_time('F.Y'); ?></dd>
                        </dl>
                    </a>
                </li>
                <?php endwhile; ?>
                <?php endif; ?>
            </ul>
        </div>
        <div class="main-more-btn js-anime moreread" id="next">
            <a class="button-wpl">
                <span>More</span>
            </a>
        </div>
    </div>
    <!-- /main -->

    <!-- link -->
    <div class="link single">
        <div class="inner-1100">
        <div class="link-list">
            <div class="link-item js-anime">
                <dl>
                    <dt>Contact</dt>
                    <dd>下記ページのフォームより<br class="sp">お気軽にお問い合わせください</dd>
                </dl>
                <div class="link-btn"><a href="/contact/" class="button-sd"><span>More</span></a></div>
            </div>
        </div>
        </div>
    </div>
    <!-- /link -->

</div>
<!-- /container -->

</div>
<?php get_footer() ;?>