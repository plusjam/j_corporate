		<footer>
			<div class="inner footer">
				<div class="footer-contents">
					<div class="footer-image"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" width="134" height="58" alt="プラスジャム"></div>
					<div class="footer-info">
						<div class="footer-address">
							〒153-0052<br>東京都目黒区祐天寺2-13-4 Pointline Yutenji 302<br>Pointline Yutenji 302, 2-13-4, Yutenji, Meguroku<br>153-0052 Tokyo, Japan
						</div>
						<div class="footer-copy">Copyright ©+Jam INC.ALL RIGHTS RESERVED.</div>
					</div>
				</div>
			</div>
			<div class="footer-link">
				<div>
					<a href="https://note.com/plusjam/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/note_logo_white.png">note</a><a href="https://www.facebook.com/plusjam/" target="_blank">Facebook</a>
				</div>
				<div>
					<a href="/privacy-policy/">Privacy Policy</a>
					<a href="/cookie-policy/">Cookie Policy</a>
				</div>
			</div>
		</footer>
		<div class="load">
			<div class="load-spinner">
				<div class="load-dot1"></div>
				<div class="load-dot2"></div>
				<div class="load-dot3"></div>
			</div>
		</div>
		<div id="gdpr_cookie" position="bottom" button="同意する" name="gdpr_allow" cross="false">当サイトでは、お客様により良いサービスを提供するため、クッキーを利用しています。使用に同意いただける場合は「同意する」ボタンをクリックし、クッキーに関する情報や設定については<a href="/cookie-policy/">クッキーポリシー</a>をご覧ください。</div>
	<?php wp_footer(); ?>
	<script> objectFitImages(); </script>
	<script type="text/javascript" id="worksScript">
	$(function(){
			var maxpage = <?php echo $wp_query->max_num_pages; ?>;
			var currentpage = <?php echo get_query_var('paged'); ?>;
			var prepage = <?php echo get_query_var('paged'); ?> - 1;
			var splitUrl = location.href.split('/');
			var getNum = splitUrl[splitUrl.length -2]
			var getPaged = getNum.replace(/[^0-9]/g, '');
			if(!getPaged){
				getPaged = 1;
			} 
		
		$('#next a').click(function(){ // 次ページへのリンクボタン
			currentpage ++;
			getPaged ++;

			if( currentpage >= maxpage ){ //最後のページ
				$('#next a').hide(); //次ページのリンクを隠す
				}
			return false;
		});

		if( currentpage >= maxpage ){ //最後のページ
				$('#next a').hide(); //次ページのリンクを隠す
		}
	})
	</script>
</body>
</html>