jQuery(function(){

    jQuery(".menu-open-btn").on("click", function(){
        jQuery(this).toggleClass("active");
        jQuery("nav").toggleClass("active");
        jQuery(".menu-bg").toggleClass("active");
    });

    jQuery(".menu-bg").on("click", function(){
        jQuery(".menu-open-btn").removeClass("active");
        jQuery("nav").removeClass("active");
        jQuery(this).removeClass("active");
    });

    // nav current
    let path = location.pathname;
    jQuery(".navlink").each(function(){
        if(path.indexOf(jQuery(this).data("name")) >= 0){
            jQuery(this).parent("li").addClass("current");
        }
        if(jQuery(this).data("name") === "top" && path === "/"){
            jQuery(this).parent("li").addClass("current");
        }
    });

    // アコーディオン
    jQuery(".ac-head").on("click",function(){
		jQuery(this).next(".ac-contents").slideToggle(350).toggleClass("open");
		jQuery(this).toggleClass("open");
        jQuery(this).closest(".solution-more").toggleClass("open");
	});

    // ページ内リンク　スルーっと移動
    jQuery('a[href^="#"]' + 'a:not(".carousel-control")').click(function(){
        var aboutUrl = 'https://' + location.host + '/about/';
        var recruitUrl = 'https://' + location.host + '/recruit/';
        if(location.href.match(aboutUrl) || location.href.match(recruitUrl)){
        var speed = 500;
        var href= jQuery(this).attr("href");
        var target = jQuery(href == "#" || href == "" ? 'html' : href);
        var position = target.offset().top;
        jQuery("html, body").animate({scrollTop:position}, speed, "swing");
        return false;
        }
    });

    // members-item ランダム表示
    var arr = [];
    jQuery(".members-item").each(function() {
        arr.push(jQuery(this).html());
    });
    arr.sort(function() {
        return Math.random() - Math.random();
    });
    var memberUl = jQuery(".members-list ul")
    memberUl.empty();
    for(i=0; i < arr.length; i++) {
        memberUl.append('<li class="members-item js-anime">' + arr[i] + '</li>');
    }

    // members-item クリック
    var membersDt = jQuery(".recruit .members-item dl dt");
    membersDt.addClass("off");
    membersDt.on("click",function(){
        jQuery(this).toggleClass("on");
        jQuery(this).toggleClass("off");

        if(membersDt.hasClass("on")){
            membersDt.removeClass("on");
        }
	});
    jQuery(window).resize(function() {
        membersDt.addClass("off");
        membersDt.removeClass("on");
    });

    // works詳細ページ
    jQuery(function(){
        var mainContent = jQuery('.work-main-content');
        var figure = mainContent.children('figure');
        var p = mainContent.children('p');

        figure.replaceWith(function(){
        var tag_class = jQuery(this).attr('class');
        var largeImg = jQuery(this).hasClass('is-style-large');
        var smallImg = jQuery(this).hasClass('is-style-medium');
        // var checkWidth = jQuery(this).find('img').width()
    
            if(largeImg){
                if(tag_class == null){
                    var tag_class_plus = [];
                } else {
                    var tag_class_plus = ' class="main-imgl js-anime'+ '"';
                }
            } else if(smallImg){
                if(tag_class == null){
                    var tag_class_plus = [];
                } else {
                    var tag_class_plus = ' class="main-imgs js-anime'+ '"';
                }    
            }
        jQuery(this).replaceWith('<div' + tag_class_plus + '>' +  jQuery(this).html() + '</div>')
        })
        p.wrap('<div class="main-text js-anime">');
    });

    // contact errorページ
    jQuery(function(){
        var errorUrl = 'https://' + location.host + '/contact/error/'
        if(location.href == errorUrl){
            var errorText = jQuery('.error-text');
            var parent = jQuery('#mw_wp_form_mw-wp-form-30');

            if(parent.find('.error-text')){
                errorText.css('display','unset');
                errorText.parents('div').addClass('is-error');
            }
        }
    });

    // contact confirmページ
    jQuery(function(){
        var confirmUrl = 'https://' + location.host + '/contact/confirm/'
        if(location.href == confirmUrl){
            var parent = jQuery('#mw_wp_form_mw-wp-form-30');

            parent.find('label').css('color','#666666');
            jQuery('span.label').css({'color':'#000','display':'block'});

        }
    });
    
    // Works詳細ページ Backボタン
    if(location.href.match('/works/')){
        jQuery('.button-sd').on('click', function(){
            var ref = document.referrer;
            var fromHome = location.host;
            var fromWorks = location.host + "/works/";
    
            if(ref.match(fromWorks)){
                history.back();
                return;
            } else {
                location = 'https://' + location.host + '/works/';
            }
        })
    }

    // ajax moreボタン
    function isAjax(){
        jQuery('.button-wpl').on('click',function(){
            if(location.pathname.match('/website/')){
                var ajaxUrl = '/works/website/page/'
            } else if(location.pathname.match('/system/')){
                var ajaxUrl = '/works/system/page/'
            } else if(location.pathname.match('/ec/')){
                var ajaxUrl = '/works/ec/page/'
            } else if(location.pathname.match('/other/')){
                var ajaxUrl = '/works/other/page/'
            } else if(location.pathname.match('/works/')){
                var ajaxUrl = '/works/page/'
            }
            var splitUrl = location.href.split('/');
            var getNum = splitUrl[splitUrl.length -2]
            var getPaged = getNum.replace(/[^0-9]/g, '');
            if(!getPaged){
                getPaged = 1;
            } 
            getPaged ++;
            $.ajax(
            {
                type: "GET",
                url: ajaxUrl + getPaged + '/?all=all',
                dataType: "html"
            })
            //通信が成功した場合の処理
            .done(function(data) { 
                var worksItem = jQuery(data).find('.works-item');
                jQuery('.works-list').append(worksItem);
                history.replaceState('','',ajaxUrl + getPaged+'/');

            })
            // Ajax通信が失敗したら発動
            .fail(function(jqXHR, textStatus, errorThrown){
                alert('Ajax通信に失敗しました。');
                console.log("jqXHR          : " + jqXHR.status); // HTTPステータスを表示
                console.log("textStatus     : " + textStatus);    // タイムアウト、パースエラーなどのエラー情報を表示
                console.log("errorThrown    : " + errorThrown.message); // 例外情報を表示
            })
            // Ajax通信が成功・失敗のどちらでも発動
            .always(function(data){
                jQuery(".js-anime").each(function(){
                    var elemPos = jQuery(this).offset().top;
                    var scroll = jQuery(window).scrollTop();
                    var windowHeight = jQuery(window).height();
                    if (scroll > elemPos - windowHeight + 0){
                        jQuery(this).addClass("on");
                    }
                });
            });
        });
    } 

    isAjax();

    // ajax カテゴリーソート
    jQuery('.main-category li').on('click',function(){
        jQuery(".main-category li.active").removeClass("active");
        jQuery(this).addClass("active");

        var index = $('.main-category li').index(this);
        
        if(index == 0){
            var afterUrl = '/works/'
        } else if(index == 1){
            var afterUrl = '/works/website/'
        } else if(index == 2){
            var afterUrl = '/works/system/'
        } else if(index == 3){
            var afterUrl = '/works/ec/'
        } else if(index == 4){
            var afterUrl = '/works/other/'
        }

        $.ajax(
        {
            type: "GET",
            url: 'https://' + location.host + afterUrl,
            dataType: "html"
        })
        //通信が成功した場合の処理
        .done(function(data) { 
            var worksItem = jQuery(data).find('.works-item');
            var button = jQuery(data).find('.main-more-btn');
            var jsScript = $(data).find().prevObject;
            var newScript= jsScript.slice(-2)[0];
            
            jQuery('.works-item').fadeOut(400).queue(function() {this.remove();});
            jQuery('.main-more-btn').remove();
            jQuery('#worksScript').remove();
            jQuery('.works-list').html(worksItem);
            jQuery('.main').append(button);
            jQuery('body').append(newScript);
            history.replaceState('','',afterUrl);
        })
        // Ajax通信が失敗したら発動
        .fail(function(jqXHR, textStatus, errorThrown){
            alert('Ajax通信に失敗しました。');
            console.log("jqXHR          : " + jqXHR.status); // HTTPステータスを表示
            console.log("textStatus     : " + textStatus);    // タイムアウト、パースエラーなどのエラー情報を表示
            console.log("errorThrown    : " + errorThrown.message); // 例外情報を表示
        })
        // Ajax通信が成功・失敗のどちらでも発動
        .always(function(data){
            jQuery(".js-anime").each(function(){
                var elemPos = jQuery(this).offset().top;
                var scroll = jQuery(window).scrollTop();
                var windowHeight = jQuery(window).height();
                if (scroll > elemPos - windowHeight + 0){
                    jQuery(this).addClass("on");
                }
            });
            isAjax();
        });
    });

    // 格カテゴリーページ カテゴリーナビCSS対応
    jQuery(function(){
        var worksUrl = 'https://' + location.host + '/works/' || 'https://' + location.host + '/works/page/';
        var websiteUrl = '/website/';
        var systemUrl = '/system/';
        var ecUrl = '/ec/';
        var otherUrl = '/other/';
        var here = location.href;
    
        if(here.match(websiteUrl)){
            $('.main-category').children('li:nth-of-type(2)').addClass('active');
        } else if(here.match(systemUrl)){
            $('.main-category').children('li:nth-of-type(3)').addClass('active');
        } else if(here.match(ecUrl)){
            $('.main-category').children('li:nth-of-type(4)').addClass('active');
        } else if(here.match(otherUrl)){
            $('.main-category').children('li:nth-of-type(5)').addClass('active');
        } else if(here.match(worksUrl)){
            $('.main-category').children('li:nth-of-type(1)').addClass('active');
        }
    })

    // works-detail メンバーの並び
    var memberList = jQuery(".member-list ul li");
    if(memberList.length < 4){
        memberList.parent().addClass("center");
    }

    // googlemapリンク　PC,タブレットで分岐
    var ua = navigator.userAgent;
    if(ua.indexOf('iPhone') > 0 || ua.indexOf('iPod') > 0 || ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0){
        
    }else if(ua.indexOf('iPad') > 0 || ua.indexOf('Android') > 0){
        jQuery('#google_link').attr('target', '_blank');
        jQuery('#google_link').attr('rel','noopener');
    }else{
        jQuery('#google_link').attr('target', '_blank');
        jQuery('#google_link').attr('rel','noopener');
    }

    //page-top-btnボタン
    jQuery('#page-top').hide();
    jQuery(window).scroll(function(){
        if (jQuery(this).scrollTop() > 200) {
            jQuery('#page-top').fadeIn(500);
        }else {
            jQuery('#page-top').fadeOut(500);
        }
    });
	jQuery('#page-top').click(function(){
		jQuery("body,html").animate({scrollTop:0},500);
	});

});

window.onload = function() {

    //load-animation
    jQuery(".load").addClass("loaded");

    // js-anime
	scroll_effect(0);
	jQuery(window).scroll(function(){
		scroll_effect(100);
	});

	function scroll_effect(hight){
		jQuery(".js-anime").each(function(){
			var elemPos = jQuery(this).offset().top;
			var scroll = jQuery(window).scrollTop();
			var windowHeight = jQuery(window).height();
			if (scroll > elemPos - windowHeight + hight){
				jQuery(this).addClass("on");
			}
		});

        jQuery(".js-anime-loop").each(function(){
			var elemPos = jQuery(this).offset().top;
			var scroll = jQuery(window).scrollTop();
			var windowHeight = jQuery(window).height();
			if (scroll > elemPos - windowHeight + hight){
				jQuery(this).addClass("on");
			}else{
                jQuery(this).removeClass("on");
            }
		});
	}

    // PCナビ スクロール
    const fixedNav = jQuery("nav"),
        height = 300;
    let offset = 0,
        lastPosition = 0,
        ticking = false;
        
    function onScroll() {
        if (lastPosition > height) {
            if (lastPosition > offset) {
                fixedNav.addClass("hide");
                fixedNav.removeClass("show");
            } else if(lastPosition < offset){
                fixedNav.removeClass("hide");
                fixedNav.addClass("show");
            }
            offset = lastPosition;
        }
    }
    jQuery(window).on("scroll",function(){
        lastPosition = window.pageYOffset;
        if (!ticking) {
        window.requestAnimationFrame(function() {
            onScroll(lastPosition);
            ticking = false;
        });
        ticking = true;
        }
    });
};
