<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="icon" href="<?php echo esc_url( home_url( '/', 'https' ) ); ?>wordpress/wp-content/uploads/2019/10/favicon.ico" sizes="32x32" />
	<link rel="icon" href="<?php echo esc_url( home_url( '/', 'https' ) ); ?>wordpress/wp-content/uploads/2019/10/favicon.ico" sizes="192x192" />
	<link rel="apple-touch-icon-precomposed" href="<?php echo esc_url( home_url( '/', 'https' ) ); ?>wordpress/wp-content/uploads/2019/10/favicon.ico" />
    <meta name="msapplication-TileImage" content="<?php echo esc_url( home_url( '/', 'https' ) ); ?>wordpress/wp-content/uploads/2019/10/favicon.ico" />
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-MMP5ZFB');</script>
	<!-- End Google Tag Manager -->
	<?php wp_head(); ?>
	<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/js/app.js'></script> 
	<script src="https://kit.fontawesome.com/d200b0e42d.js" crossorigin="anonymous"></script> 
	<script type='text/javascript' src="<?php echo get_template_directory_uri(); ?>/js/ofi.min.js"></script>
	<script defer src="<?php echo get_template_directory_uri(); ?>/js/gdpr.js"></script>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/gdpr.css" type="text/css" >
</head>
<body >
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MMP5ZFB"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<header class=" <?php echo is_home() === true ? 'header-top' : 'header'; ?>">
		<div class="menu">
			<div class="menu-open-btn">
				<span></span>
				<span></span>
				<span></span>
			</div>
		</div>
		<div class="menu-bg"></div>
		<div class="header-logo"><a href="<?php echo home_url() ;?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logo-b.png" width="110" height="48" alt="プラスジャム"></a></div>
		<nav>
		<div class="nav-logo"><a href="<?php echo home_url() ;?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logo-b.png" width="110" height="48" alt="プラスジャム"></a></div>

			<ul>
				<li class=""><a href="<?php echo home_url(); ?>" data-content="Top" data-name="top" class="navlink">Top<span>トップ</span></a></li>
				<li><a href="/service/" data-content="Service" data-name="service" class="navlink">Service<span>事業内容</span></a></li>
				<li><a href="/works/" data-content="Works" data-name="works" class="navlink">Works<span>事例紹介</span></a></li>
				<li><a href="/about/" data-content="About" data-name="about" class="navlink">About<span>企業紹介</span></a></li>
				<li><a href="/recruit/" data-content="Recruit" data-name="recruit" class="navlink">Recruit<span>採用情報</span></a></li>
				<li><a href="/contact/" data-content="Contact" data-name="contact" class="navlink">Contact<span>お問合せ</span></a></li>
			</ul>
			<!-- <ul class="sp">
				<li><a href="/privacy-policy/">Privacy Policy</a></li>
				<li><a href="https://www.facebook.com/plusjam/" target="_blank">Facebook</a></li>
			</ul> -->
			<div class="nav-link">
				<div>
					<a href="https://note.com/plusjam/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/note_logo_black.png">note</a><a href="https://www.facebook.com/plusjam/" target="_blank">Facebook</a>
				</div>
				<!-- <div>
					<a href="/privacy-policy/">Privacy Policy</a>
				</div> -->
			</div>
		</nav>
	</header>
	<!-- page-top-btn -->
	<div id="page-top"><div></div></div>
	<!-- /page-top-btn -->
