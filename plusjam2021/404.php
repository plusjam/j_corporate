<?php get_header() ;?>
<div class="wrapper">

<!-- container -->
<div class="container error">

    <!-- lead -->
    <section class="lead">
        <div class="inner-1620">
        <div class="multi-title js-anime">
            <h1><span>4</span><span>0</span><span>4</span><span>&nbsp;</span><span>E</span><span>r</span><span>r</span><span>o</span><span>r</span></h1>
            <p>NOT FOUND</p>
        </div>
        </div>
    </section>
    <!-- /lead -->

    <!-- main -->
    <div class="main">
        <div class="inner-1100">
        <div class="main-contents">
            <div class="main-image js-anime">
                <img src="<?php echo get_template_directory_uri(); ?>/images/error_404.gif" alt="">
            </div>
            <dl class="main-text">
                <dt class="js-anime">Sorry, The page you're looking for couldn't be found.</dt>
                <dd class="js-anime">お探しのページは見つかりませんでした。<br>一時的にアクセスできない状態か、削除もしくは移動された可能性があります。</dd>
            </dl>
            <div class="main-btn-home"><a href="<?php echo home_url(); ?>" class="button-sd js-anime"><span>Home</span></a></div>
        </div>
        </div>
    </div>
    <!-- /main -->

</div>
<!-- /container -->

</div>

<?php get_footer() ;?>