<?php get_header() ;?>
	<div class="wrapper" id="luxy">

		<!-- container -->
		<div class="container top">
			<!-- Main visual -->
			<div class="mv js-anime">
				<div class="mv-contents">
					<h1 class="mv-logo js-anime"><img src="<?php echo get_template_directory_uri() ;?>/images/logo.png" width="263" height="111" alt="プラスジャム"></h1>
					<h2 class="mv-catch js-anime"><span>Web Creative,</span><span>&nbsp;System,</span><span>&nbsp;Marketing</span><br><span>&nbsp;+&nbsp;Humaneness</span></h2>
				</div>
				<div class="mv-bg js-anime">
					<img src="<?php echo get_template_directory_uri() ;?>/images/top_mv01.png" width="935" height="739" class="pc" alt="">
					<img src="<?php echo get_template_directory_uri() ;?>/images/top_mv01-w.png" width="935" height="739" class="pc mask" alt="">
					<img src="<?php echo get_template_directory_uri() ;?>/images/top_mv01-sp.png" width="750" height="719" class="sp" alt="">
					<span class="mv-circle"></span>
					<span class="mv-circle"></span>
					<span class="mv-circle"></span>
					<span class="mv-circle"></span>
				</div>
				<div class="mv-side js-anime">plusjam.inc<span></span></div>
			</div>
			<!-- /Main visual -->

			<div class="decoration">
				<div class="decoration-parts inner-1500">
					<div class="decoration-cross">
						<span class="js-anime"></span>
						<span class="js-anime"></span>
						<span class="js-anime"></span>
						<span class="js-anime"></span>
						<span class="js-anime"></span>
						<span class="js-anime"></span>
					</div>
					<div class="decoration-crossb">
						<span class="js-anime"></span>
						<span class="js-anime"></span>
						<span class="js-anime"></span>
						<span class="js-anime"></span>
						<span class="js-anime"></span>
						<span class="js-anime"></span>
					</div>
				</div>
			</div>

			<!-- About us -->
			<section class="about">
				<div class="inner-1500">
				<div class="about-image js-anime">
					<h2 class="js-anime"><img src="<?php echo get_template_directory_uri() ;?>/images/top_about02.png" alt="About us"></h2>
					<div class="js-anime"><img src="<?php echo get_template_directory_uri() ;?>/images/top_about01.jpg" alt="About us"></div>
				</div>
				<div class="about-contents">
					<h3 class="about-catch js-anime"><img src="<?php echo get_template_directory_uri() ;?>/images/top_about03.png" width="370" height="34" alt="向き合い、明るい未来へ"></h3>
					<p class="about-text js-anime">プラスジャムは、サイト制作、システム開発、マーケティングなど、さまざまな課題解決やアイディアを具現化するWebソリューションを提案・提供しています。</p>
					
				</div>
				<div class="about-btn"><a href="/about/" class="button-sd js-anime-loop"><span>About US</span></a></div>
				</div>
			</section>
			<!-- /About us -->

			<!-- Works -->
			<section class="works js-anime-loop">
				<div class="inner">
					<div class="multi-title js-anime">
						<h2><span>W</span><span>o</span><span>r</span><span>k</span><span>s</span></h2>
						<p>事例紹介</p>
					</div>
					<ul class="works-list">
					<?php if ( have_posts() ) : ?>
						<?php while ( have_posts() ) : the_post(); ?>
						<li class="js-anime">
							<a href="<?php the_permalink() ?>">
								<div>
								<?php 
								$pcThumbnail = get_field('pc_thumbnail');
								if($pcThumbnail){ 
								?>
									<img src="<?php echo esc_url($pcThumbnail['url']) ?>" alt="<?php echo esc_attr($pcThumbnail['alt']) ?>">
										
								<?php }; ?>
								</div>
								<dl>
									<dt><?php the_title(); ?></dt>
									<dd><?php echo get_post_time('F.Y'); ?></dd>
								</dl>
							</a>
						</li>
						<?php endwhile;?>
					<?php endif; ?>
					</ul>
					<div class="works-btn"><a href="/works/" class="button-sd js-anime-loop"><span>More</span></a></div>
				</div>
			
			</section>
			<!-- /Works -->

			<!-- Service -->
			<section class="service">
				<div class="inner">
				<div class="multi-title js-anime">
					<h2><span>S</span><span>e</span><span>r</span><span>v</span><span>i</span><span>c</span><span>e</span></h2>
					<p>事業案内</p>
				</div>
				</div>

				<div class="inner-1500">
				<div class="service-image js-anime">
					<div class="">
						<img src="<?php echo get_template_directory_uri() ;?>/images/top_service01.jpg" alt="事業案内" class="">
					</div>
				</div>
				<div class="service-contents">
					<ul class="service-icons">
						<li class="js-anime-loop"><a href="/service/website/"><dl>
							<dt><img src="<?php echo get_template_directory_uri() ;?>/images/top_service02.png" width="65" height="68"alt="WEBサイト制作"></dt>
							<dd>Web Site Creation</dd><dd>WEBサイト制作</dd>
						</dl></a></li>
						<li class="js-anime-loop"><a href="/service/system/"><dl>
							<dt><img src="<?php echo get_template_directory_uri() ;?>/images/top_service03.png" width="65" height="62" alt="システム開発"></dt><dd>System development</dd><dd>システム開発</dd>
						</dl></a></li>
						<li class="js-anime-loop"><a href="/service/ec/"><dl>
							<dt><img src="<?php echo get_template_directory_uri() ;?>/images/top_service04.png" width="61" height="60" alt="ECサイト制作"></dt><dd>E-commerce</dd><dd>ECサイト制作</dd>
						</dl></a></li>
						<li class="js-anime-loop"><a href="/service/analytics/"><dl>
							<dt><img src="<?php echo get_template_directory_uri() ;?>/images/top_service05.png" width="76" height="66" alt="サイト分析/戦略"></dt><dd>Analysis/Strategy</dd><dd>サイト分析/戦略</dd>
						</dl></a></li>
						<li class="js-anime-loop"><a href="/service/marketing/"><dl>
							<dt><img src="<?php echo get_template_directory_uri() ;?>/images/top_service06.png" width="74" height="64" alt="マーケティング/運用"></dt><dd>Marketing/Operations</dd><dd>マーケティング/運用</dd>
						</dl></a></li>
					</ul>
					<div class="service-btn"><a href="/service/" class="button-sd js-anime-loop"><span>More</span></a></div>
				</div>
				</div>
			</section>
			<!-- /Service -->

			<!-- Recruit -->
			<section class="recruit">
				<div class="inner-1500">
				<div class="multi-title js-anime">
					<h2><span>R</span><span>e</span><span>c</span><span>r</span><span>u</span><span>i</span><span>t</span></h2>
					<p>採用情報</p>
				</div>

				<div class="recruit-image js-anime">
					<img src="<?php echo get_template_directory_uri() ;?>/images/top_recruit01.jpg" alt="採用情報" class="">
				</div>

				<div class="recruit-contents js-anime">
					<p>ブラスジャムは「自分らしい働き方」を叶えるフィールドです。<br class="pc">私たちのスタイルや価値観を共有できる仲間を募集しています。</p>
					<div class="recruit-btn"><a href="/recruit/" class="button-sd js-anime-loop"><span>More</span></a></div>
				</div>
				</div>
			</section>
			<!-- /Recruit -->
		</div>
        <!-- /container -->
<?php get_footer() ;?>

