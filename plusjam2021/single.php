<?php get_header() ;?>
<div class="wrapper">

<!-- container -->
<div class="container works-detail">

    <!-- main-->
    <section class="main">
        <div class="inner-1100">
        <div class="main-title js-anime">
            <div><?php echo get_post_time('F.Y'); ?></div>
            <h1><?php the_title(); ?></h1>
        </div>
        </div>
        <div class="inner-1100 image">
            <div class="main-image js-anime">
            <!-- PCメイン画像 -->
            <?php 
            $pcThumbnail = get_field('pc_thumbnail');
            if($pcThumbnail){ 
            ?>
                <img src="<?php echo esc_url($pcThumbnail['url']) ?>" alt="<?php echo esc_attr($pcThumbnail['alt']) ?>" class="pc">
                    
            <?php }; ?>
            <!-- SPメイン画像 -->
            <?php 
            $spThumbnail = get_field('sp_thumbnail');
            if($spThumbnail){ 
            ?>
                <img src="<?php echo esc_url($spThumbnail['url']) ?>" alt="<?php echo esc_attr($spThumbnail['alt']) ?>" class="sp">
                    
            <?php }; ?>
                <!-- <?php if(has_post_thumbnail()): ?>
                <?php the_post_thumbnail(); ?>
                <?php else: ?>
                <img src="<?php echo get_template_directory_uri() ;?>/images/top_works01.jpg" alt="画像がありません">
                <?php endif; ?> -->
                <!-- <img src="<?php echo get_template_directory_uri() ;?>/images/works_detail01.jpg" alt="" class="pc">
                <img src="<?php echo get_template_directory_uri() ;?>/images/works_detail01-sp.jpg" alt="" class="sp"> -->
            </div>
        </div>
        <div class="inner-1100">
        <div class="main-contents js-anime">
            <p><?php echo post_custom('main_description'); ?></p>
            <div>
                <dl>
                    <dt>
                    <?php the_category(', '); ?>
                    <dd>
                    <?php 
                        $posttags = get_the_tags();
                            if ( $posttags ) {
                                echo '<ul>';
                                foreach ( $posttags as $tag ) {
                                    echo '<li>'.$tag->name.'</li>';
                                }
                                echo '</ul>';
                            }
                        ?>
                    </dd>
                </dl>
            </div>
        </div>
        <div class="main-link">
        <?php if(get_post_meta($post->ID, 'site_link',true)): // 入力がある場合 ?>
        <?php echo '<a href="' .post_custom('site_link'). '" target="_blank">Visit</a>'; ?>
        <?php endif; ?>
        </div>
        <div class="work-main-content">
            <?php the_content() ?>
        </div>
        </div>
    </section>
    <!-- /main -->

    <!-- member -->
    <section class="member">
    <?php
        $users = get_field("user");
        // $users = get_users();
        if( $users ): ?>
        <h2 class="js-anime">Project Member</h2>
        <div class="member-list js-anime">
            <ul class="">
                <?php foreach( $users as $user ): 
                $author_id = $user['ID'];
                $author_badge = get_field('profile_image', 'user_'. $author_id ); ?>
                
                <li>
                    <div><img src="<?php echo $author_badge['url']; ?>" alt="<?php echo $author_badge['alt']; ?>" /></div>
                    <p><?php echo $user['display_name']; ?></p> 
                </li>
                 <?php endforeach; ?>
            </ul>
        </div>
        <?php endif; ?>
        <div class="member-btn js-anime"><a class="button-sd" ><span>Works List</span></a></div>
    </section>
    <!-- / member -->

    <!-- link -->
    <div class="link single">
        <div class="inner-1100">
        <div class="link-list">
            <div class="link-item js-anime">
                <dl>
                    <dt>Contact</dt>
                    <dd>下記ページのフォームより<br class="sp">お気軽にお問い合わせください</dd>
                </dl>
                <div class="link-btn"><a href="/contact/" class="button-sd"><span>More</span></a></div>
            </div>
        </div>
        </div>
    </div>
    <!-- /link -->

</div>
<!-- /container -->

</div>

<?php get_footer() ;?>