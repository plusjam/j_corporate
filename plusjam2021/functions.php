<?php
/**
 * WordPress標準機能
 */
function my_setup() {
  add_theme_support( 'post-formats' ); /* ポストフォーマット */
  add_theme_support( 'automatic-feed-links' ); /* RSSフィード */
  add_theme_support( 'custom-background' ); /* カスタムバックグラウンド */
  add_theme_support( 'custom-header' ); /* カスタムヘッダー */
  add_theme_support( 'title-tag' ); /* タイトルタグ自動生成 */

  add_theme_support( 'html5', array( /* HTML5のタグで出力 */
    'search-form',
    'comment-form',
    'comment-list',
    'gallery',
    'caption',
  ) );

  add_theme_support( 'menus' ); /* メニュー生成 */
    //   register_nav_menus(
    //     array(
    //         'menu' => 'メニュー',
    //         'footer' =>　'フッター',
    //         'social' => 'ソーシャル',
    //     )
    // );
}
add_action( 'after_setup_theme', 'my_setup' );

/* 【管理画面】投稿編集画面で不要な項目を非表示にする */
function my_remove_meta_boxes() {
  remove_meta_box('postexcerpt','post','normal' );      // 抜粋
  remove_meta_box('trackbacksdiv','post','normal' );    // トラックバック
  remove_meta_box('commentsdiv','post','normal' );      // コメント
  remove_meta_box('commentstatusdiv','post','normal' ); // ディスカッション
}
add_action('admin_menu','my_remove_meta_boxes' );


/**
 * CSSとJavaScriptの読み込み
 */
function my_script_init() {
    wp_enqueue_style( 'style-name', get_template_directory_uri() . '/style.css' );
    // wp_enqueue_script( 'script-name', get_template_directory_uri() . '/js/app.js' );
  }
  add_action( 'wp_enqueue_scripts', 'my_script_init' );

  
// /**
//  * 日本語環境で日付フォーマットの設定を英語表記にするフィルターフック
//  */
// add_filter( 'date_i18n', 'ja_date_i18n', 10, 4 );
// function ja_date_i18n( $date , $req_format, $i, $gmt){
//     if( defined('WPLANG') and WPLANG == 'ja' ) {
//         // formatがcの場合、+00:00を+09:00に修正する
//         $format= sprintf( '%1$02d:00', get_option( 'gmt_offset' ) );
//         $date = str_replace( '+00:00', '+'.$format, $date );
         
//         // 日本語環境で、フォーマットに英語表記を使用する
//         if ( preg_match( '![SlDFM]!', $req_format ) ) {
//             return apply_filters( 'ja_date_i18n', date( $req_format, $i ) );
//         }
//     }
//     return apply_filters( 'ja_date_i18n', $date );
// }




/**
 * head内に出力される不要なコードを削除
 */
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'wp_shortlink_wp_head');
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'feed_links_extra', 3);




/**
 * 投稿のテキストを変更
 */
function change_post_menu_label() {
  global $menu;
  global $submenu;
  $menu[5][0] = 'Works';
  $submenu['edit.php'][5][0] = 'Works一覧';
  $submenu['edit.php'][10][0] = 'Works追加';
  $submenu['edit.php'][16][0] = 'タグ';
  //echo ";
 }
 function change_post_object_label() {
  global $wp_post_types;
  $labels = &$wp_post_types['post']->labels;
  $labels->name = 'Works';
  $labels->singular_name = 'Work';
  $labels->add_new = _x('追加', 'Works');
  $labels->add_new_item = 'Worksの新規追加';
  $labels->edit_item = 'Worksの編集';
  $labels->new_item = '新規Works';
  $labels->view_item = 'Worksを表示';
  $labels->search_items = 'Worksを検索';
  $labels->not_found = '記事が見つかりませんでした';
  $labels->not_found_in_trash = 'ゴミ箱に記事は見つかりませんでした';
 }
 add_action( 'init', 'change_post_object_label' );
 add_action( 'admin_menu', 'change_post_menu_label' );




/**
 * 権限によって管理画面表示領域が変わる
 */
 add_action( 'admin_menu', 'remove_menus' );
function remove_menus(){
  if( current_user_can( 'author' ) ){
      remove_menu_page( 'index.php' ); //ダッシュボード
      // remove_menu_page( 'edit.php' ); //投稿メニュー
      remove_menu_page( 'upload.php' ); //メディア
      remove_menu_page( 'edit.php?post_type=page' ); //ページ追加
      remove_menu_page( 'edit-comments.php' ); //コメントメニュー
      remove_menu_page( 'themes.php' ); //外観メニュー
      remove_menu_page( 'plugins.php' ); //プラグインメニュー
      remove_menu_page( 'tools.php' ); //ツールメニュー
      remove_menu_page( 'options-general.php' ); //設定メニュー
  }
}
// トリミング
add_image_size( 'top', 355, 205, true );
add_image_size( 'works', 500, 289, true );



// アイキャッチ画像下の文言
add_filter('register_post_type_args',function($args,$post_type){
    switch($post_type){
        case'post':
            $args['labels']['featured_image']        =__('メイン画像 [横幅1100px以上]','3288-visualive-example');
            $args['labels']['set_featured_image']    =__('メイン画像を追加','3288-visualive-example');
            $args['labels']['remove_featured_image']=__('メイン画像を削除','3288-visualive-example');
            break;
    }
 
    return$args;
},10,2);

// コアパターン削除
unregister_block_pattern( 'core/text-two-columns' );
unregister_block_pattern( 'core/two-buttons' );
unregister_block_pattern( 'core/two-images' );
unregister_block_pattern( 'core/text-two-columns-with-images' );
unregister_block_pattern( 'core/text-three-columns-buttons' );
unregister_block_pattern( 'core/large-header' );
unregister_block_pattern( 'core/large-header-paragraph' );
unregister_block_pattern( 'core/three-buttons' );
unregister_block_pattern( 'core/quote' );

// MW WP FORM エラーメッセージ
add_filter( 'mwform_error_message_mw-wp-form-30', 'custom_mwform_error_message', 10, 3 );
function custom_mwform_error_message( $error, $key, $rule ) {
  if ( $key === 'summary' && $rule === 'noempty' ) {
    return 'お問い合わせ内容を選択してください';
  }
  if ( $key === 'name' && $rule === 'noempty' ) {
    return 'お名前を入力してください';
  }
  if ( $key === 'e-mail' && $rule === 'noempty' ) {
    return 'メールアドレスを入力してください';
  }
  if ( $key === 'e-mail' && $rule === 'mail' ) {
    return '正しいメールアドレスを入力してください';
  }
  if ( $key === 'company' && $rule === 'noempty' ) {
    return '会社名を正しく入力してください';
  }
  if ( $key === 'phone' && $rule === 'tel' ) {
    return '電話番号を正しく入力してください';
  }
  if ( $key === 'message' && $rule === 'noempty' ) {
    return '  メッセージを入力してください
    ';
  }
  return $error;
}

add_filter( 'mwform_error_message_html', 'custom_mwform_error_message_html', 10, 2 );
function custom_mwform_error_message_html( $tag, $error ) {
  $start_tag = '<span class="error-text">';
  $end_tag = '</span>';
  return $start_tag . esc_html( $error ) . $end_tag;
}

// 投稿ページ 画像サイズ追加
add_image_size( '画像大', 1100, 0, true );
add_image_size( '画像中', 850, 0 ,true );
add_image_size( 'プロフィール画像', 270, 270 );

// 投稿ページ タグ チェックボックス化
function _re_register_post_tag_taxonomy() {
	
	$tag_slug_args = get_taxonomy('post_tag'); // returns an object
	$tag_slug_args -> hierarchical = true;
	$tag_slug_args -> meta_box_cb = 'post_categories_meta_box';
	
	register_taxonomy( 'post_tag', 'post',(array) $tag_slug_args);

}
add_action( 'init', '_re_register_post_tag_taxonomy', 1 );

// 投稿ページ 画像ブロックスタイル追加
register_block_style(
  'core/image',
  [
    'name'         => 'large',
    'label'        => '画像大',
    'inline_style' => '.is-style-width-large img {}',
  ]
);
register_block_style(
  'core/image',
  [
    'name'         => 'medium',
    'label'        => '画像中',
    'inline_style' => '.is-style-width-medium img {}',
  ]
);

// デフォルトの画像ブロックスタイル削除
unregister_block_style( 'core/image', 'default' );
unregister_block_style( 'core/image', 'rounded' );

// 登校時に使えるブロックを制限
function custom_allowed_block_types($allowed_block_types, $post) {
  if ($post->post_type === 'post') {
    $allowed_block_types = [
      'core/paragraph',           // 段落
      'core/heading',             // 見出し
      'core/image',               // 画像
      'core/columns',             // カラム
      'core/gallery',             // ギャラリー
      'core/list',                // リスト
    ];
  } 
  return $allowed_block_types;
}
add_filter('allowed_block_types', 'custom_allowed_block_types', 10, 2);

/**
 * カテゴリアーカイブのパーマリンク指定
 * @return void
 */
function customWorksRewrite(){
    add_rewrite_rule('works/([0-9]+)/?$', 'index.php?p=$matches[1]', 'top');
    add_rewrite_rule('works/(website|system|ec|other)/?$', 'index.php?category_name=$matches[1]', 'top');
    add_rewrite_rule('works/(website|system|ec|other)/page/([0-9]+)/?$', 'index.php?category_name=$matches[1]&paged=$matches[2]', 'top');
}
add_action( 'init', 'customWorksRewrite');

/**
 * get_term_linkで取得するリンクの変更
 * @param $termlink
 * @param $term
 * @param $taxonomy
 * @return mixed
 */
function customWorksTermLink($termlink, $term, $taxonomy) {
    return ($taxonomy === 'category' ? home_url('/works/'.$term->slug.'/') : $termlink);
}
add_filter('term_link', 'customWorksTermLink', 10, 3);

///**
// * WordpressのRewriteをリフレッシュ
// * ※公開時はコメントアウトすること！
// */
//function flush_rules() {
//    global $wp_rewrite;
//    $wp_rewrite->flush_rules();
//}
//add_action('init', 'flush_rules');




// ショートコード追加
function home_func() {
	return home_url();
}
add_shortcode('my_home_url','home_func'); //ホームページタグショートコード

function stylesheet_directory_func() {
	return get_stylesheet_directory_uri();
}
add_shortcode('my_stylesheet_directory_url','stylesheet_directory_func'); //スタイルシートディレクトリショートコード




// categoryページ canonical設定
function my_canonical($canonical){
  if(is_front_page() || is_home() || is_archive()){
    add_filter('aioseo_prev_link', '__return_empty_string');
    add_filter('aioseo_next_link', '__return_empty_string');
  }

  if(is_category()){
    add_filter('aioseo_prev_link', '__return_empty_string');
    add_filter('aioseo_next_link', '__return_empty_string');
    $canonical = 'https://plusjam2021.jam-dev.net/works/';
  }
  return $canonical;
}
add_filter( 'aioseo_canonical_url', 'my_canonical');

//jQuery読み込み
function custom_scripts() {
  if (!is_admin()) {
    wp_deregister_script('jquery');
    wp_enqueue_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js');
  }
}
add_action('wp_print_scripts', 'custom_scripts');